# About Me

My name is Jose Nuñez Martinez, I am 27 years old, and I currently live in Santiago de Compostela, Galicia. I really enjoy practicing sports such as football, paddle, tennis and karting.
I am a full-stack web developer who studied at HACK A BOSS school. I am passionate about technology and I am always eager to learn new things. I am currently looking for a job as a junior developer.

## Something I consider that makes me different
I believe that my problem-solving skills and curiosity to learn new things make me unique. I am also very organized and enjoy working in a team.

## What is Git for me?
For me, Git is an essential tool for version control and collaboration in software projects. It allows me to keep track of changes made to the code and to work more efficiently with other developers. I have experience using Git, GitLab and GitHub.
I believe that is a powerful tool that needs to be thoroughly understood, as it provides a lot of assistance in detecting errors and preventing loss of code.

## What is Docker for me?
Docker is a virtualization tool that allows me to create development and deployment environments. It helps me ensure portability and consistency of applications across different environments.
I have little experience with Docker, but after several classes, I am very eager to continue learning this tool.

## What is Testing for me?
To me, Testing is a fundamental practice to ensure software quality. It allows me to have clarity on the project objectives and ensure that the code meets the requirements. Additionally, I believe it saves time during the development and maintenance of large-scale projects.

## What JavaScript characteristics do I know right now?
Currently, I have knowledge of basic JavaScript features, such as variables, data types, flow control structures, and functions, as well as more advanced concepts such as object-oriented programming, promises, and asynchronous programming. I have experience with React, Next.js and I am currently learning Vue and Angular.

## What are Web Components for me?
I consider Web Components as a set of web technologies that allow the creation of reusable and custom components for web applications. They allow me to develop more scalable and maintainable applications, and improve the user experience by providing more intuitive and consistent user interfaces.

## Characteristics I consider that make me unique.
I believe my passion for technology and my desire to understand every detail of how everything works makes me unique. I truly enjoy jobs that have a practical purpose, where I can see the impact of my work in real life. I am passionate about learning new things and applying that knowledge to solve problems. I feel more motivated when I see that my work has a positive impact on the real world.